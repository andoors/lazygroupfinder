--[[
	I've had alot of help building this addon by looking on code from:
	
	World Quest Group Finder https://mods.curse.com/addons/wow/worldquestgroupfinder
	Premade Group Finder https://mods.curse.com/addons/wow/premade-group-finder
	
	Thanks alot for you good work!
	
	Best regards,
	Anders Krüger / Luuci-Ravencrest EU
]]
LGFdebug = false

LGFgroupsFound = nil
LGFfoundGroups = {}

LGFcurrentID = 0
LGFcurrentLeader = ""
LGFcurrentActivity = 0

LGFtimeSinceLastUpdate = 0

LGFautoPause = false

function ShowBestResult()
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFShowBestResult " .. LGFgroupsFound) end
	
	local HighestScoreID = nil
	local HighestScore = -1
	local FoundGroupID = 0
	
	for k, v in pairs(LGFfoundGroups) do
		if LGFfoundGroups[k] ~= nil then
			if LGFfoundGroups[k].Score > HighestScore then
				FoundGroupID = k
				HighestScoreID = LGFfoundGroups[k].Id
				HighestScore = LGFfoundGroups[k].Score
			end
		end
	end
	if HighestScoreID ~= nil then
		LGFfoundGroups[FoundGroupID] = nil
		LGFgroupsFound = LGFgroupsFound - 1
		LazyGroupFinder_GroupPopup(HighestScoreID, HighestScore)
	else
		LazyGroupFinderDisableAddon()
	end
end

function LazyGroupFinderEnableAddon()
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFLazyGroupFinderEnableAddon") end
	
	C_LFGList.ClearSearchResults()
	C_LFGList.Search(LazyGroupFinderActivityType, LFGListSearchPanel_ParseSearchTerms(""), 0, 4, C_LFGList.GetLanguageSearchFilter())
	LGFtimeSinceLastUpdate = 9
	LazyGroupFinderEnabled = true
	LazyGroupFinder_MinimapButton_SetEnabled()
end

function LazyGroupFinderDisableAddon()
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFLazyGroupFinderDisableAddon") end
	LazyGroupFinderEnabled = false
	LazyGroupFinder_MinimapButton_SetDisabled()
	for k, v in pairs(LGFfoundGroups) do LGFfoundGroups[k] = nil end
	LGFgroupsFound = nil
	LazyGroupFinderPopup:Hide()
end

local LazyGroupFinderAddon = CreateFrame("Frame", "LazyGroupFinderAddon", UIParent)
local RegisteredEvents = {}
LazyGroupFinderAddon:SetScript("OnEvent", function (self, event, ...) if (RegisteredEvents[event]) then return RegisteredEvents[event](self, event, ...) end end)

function RegisteredEvents:ADDON_LOADED(event, addon, ...)
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFRegisteredEvents:ADDON_LOADED") end

	if addon ~= "LazyGroupFinder" then return end
	if LazyGroupFinderMinimapIconPoint == nil then LazyGroupFinderMinimapIconPoint = 180 end
	if LazyGroupFinderMinimapIconHover == nil then LazyGroupFinderMinimapIconHover = false end
	if LazyGroupFinderSoundAnnouncement == nil then LazyGroupFinderSoundAnnouncement = true end
	if LazyGroupFinderChatAnnouncement == nil then LazyGroupFinderChatAnnouncement = false end
	
	if LazyGroupFinderEnabled == nil then LazyGroupFinderEnabled = false end
	if LazyGroupFinderAchievementOnApply == nil then LazyGroupFinderAchievementOnApply = false end
	if LazyGroupFinderActivityType == nil then LazyGroupFinderActivityType = 3 end
	if LazyGroupFinderActivities == nil then LazyGroupFinderActivities = {} end
	if LGF_ActivityFilter == nil then LGF_ActivityFilter = { "", "", "" } end
	if LGF_Keywords == nil then LGF_Keywords = { "", "", "" } end
	if LGF_Bannedwords == nil then LGF_Bannedwords = { "WTS", "WTS", "WTS" } end
	if LGF_MinTank == nil then LGF_MinTank = { "", "", "" } end
	if LGF_MaxTank == nil then LGF_MaxTank = { "", "", "" } end
	if LGF_MinHeal == nil then LGF_MinHeal = { "", "", "" } end
	if LGF_MaxHeal == nil then LGF_MaxHeal = { "", "", "" } end
	if LGF_MinDps == nil then LGF_MinDps = { "", "", "" } end
	if LGF_MaxDps == nil then LGF_MaxDps = { "", "", "" } end
	if LGF_MinIlvl == nil then LGF_MinIlvl = { 0, 0, 0 } end
	if LGF_ScoreFactorIlvl == nil then LGF_ScoreFactorIlvl = { 1.0, 1.0, 1.0 } end
	if LGF_ScoreFactorMember == nil then LGF_ScoreFactorMember = { 1.0, 1.0, 1.0 } end
	if LGF_ScoreFactorEncounter == nil then LGF_ScoreFactorEncounter = { 1.0, 1.0, 1.0 } end

	if not LazyGroupFinderMinimapIconHover then LGFminimapIconButton:Show() end
	LazyGroupFinderDisableAddon()
	
	LazyGroupFinder_MinimapButton_SetPoint(LazyGroupFinderMinimapIconPoint)
end

function RegisteredEvents:LFG_LIST_SEARCH_RESULTS_RECEIVED(event)
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFRegisteredEvents:LFG_LIST_SEARCH_RESULTS_RECEIVED: " .. event) end

	if not LazyGroupFinderEnabled then return end
	if LFGListFrame:IsVisible() then
		LazyGroupFinderDisableAddon()
		return
	end

	for k, v in pairs(LGFfoundGroups) do LGFfoundGroups[k] = nil end

	local numResults, results = C_LFGList.GetSearchResults()
	LFGListUtil_SortSearchResults(results)

	for k, v in pairs(results) do
		local id,activity,name,comment,_,ilvl,_,age,_,_,_,isDelisted,leader,members = C_LFGList.GetSearchResultInfo(results[k])

		if leader ~= nil then -- the group needs a leader
			local activityName,_,activityType,_,activityIlvl,_,_,_,_,_ = C_LFGList.GetActivityInfo(activity)

			local groupSetup = C_LFGList.GetSearchResultMemberCounts(id)
			local encounterInfo = C_LFGList.GetSearchResultEncounterInfo(id)
			
			if LazyGroupFinderActivities[activity] == nil then LazyGroupFinderActivities[activity] = false end

			if LazyGroupFinderActivities[activity] == true then
				
				local goodGroupSetup = true
				if LGF_MinTank[LazyGroupFinderActivityType] ~= "" then
					if tonumber(groupSetup["TANK"]) < tonumber(LGF_MinTank[LazyGroupFinderActivityType]) then goodGroupSetup = false end
				end
				if LGF_MaxTank[LazyGroupFinderActivityType] ~= "" then
					if tonumber(groupSetup["TANK"]) > tonumber(LGF_MaxTank[LazyGroupFinderActivityType]) then goodGroupSetup = false end
				end
				if LGF_MinHeal[LazyGroupFinderActivityType] ~= "" then
					if tonumber(groupSetup["HEALER"]) < tonumber(LGF_MinHeal[LazyGroupFinderActivityType]) then goodGroupSetup = false end
				end
				if LGF_MaxHeal[LazyGroupFinderActivityType] ~= "" then
					if tonumber(groupSetup["HEALER"]) > tonumber(LGF_MaxHeal[LazyGroupFinderActivityType]) then goodGroupSetup = false end
				end
				if LGF_MinDps[LazyGroupFinderActivityType] ~= "" then
					if tonumber(groupSetup["DAMAGER"]) < tonumber(LGF_MinDps[LazyGroupFinderActivityType]) then goodGroupSetup = false end
				end
				if LGF_MaxDps[LazyGroupFinderActivityType] ~= "" then
					if tonumber(groupSetup["DAMAGER"]) > tonumber(LGF_MaxDps[LazyGroupFinderActivityType]) then goodGroupSetup = false end
				end
				
				if goodGroupSetup
					and IsKeywordsInWords(LGF_Keywords[LazyGroupFinderActivityType], name .. " " .. comment .. " " .. leader)
					and IsKeywordsNotInWords(LGF_Bannedwords[LazyGroupFinderActivityType], name .. " " .. comment .. " " .. leader) 
					and ilvl >= tonumber(LGF_MinIlvl[LazyGroupFinderActivityType]) then
					local index = #LGFfoundGroups + 1
					LGFfoundGroups[index] = {}
					LGFfoundGroups[index].Id = id
					LGFfoundGroups[index].Name = name
					LGFfoundGroups[index].Leader = leader
					LGFfoundGroups[index].Activity = activity
					local encounterScore, lootableEncounters, encountersLeft = GetEncounterScore(activity, encounterInfo)
					if lootableEncounters == 0 then encounterScore = 0 end
					LGFfoundGroups[index].Score = math.floor(
						max(ilvl, activityIlvl) * LGF_ScoreFactorIlvl[LazyGroupFinderActivityType]
						+ members * LGF_ScoreFactorMember[LazyGroupFinderActivityType]
						+ encounterScore * activityIlvl * LGF_ScoreFactorEncounter[LazyGroupFinderActivityType])
				end
			end
		end
	end
	
	LGFgroupsFound = #LGFfoundGroups
	FlashClientIcon()
	if LazyGroupFinderChatAnnouncement then
		if #LGFfoundGroups == 1 then
			DEFAULT_CHAT_FRAME:AddMessage("|cFFFFFFFFLazy Group Finder:|cFFFFFF00 " .. #LGFfoundGroups .. " group found.")
		else
			DEFAULT_CHAT_FRAME:AddMessage("|cFFFFFFFFLazy Group Finder:|cFFFFFF00 " .. #LGFfoundGroups .. " groups found.")
		end
	end
	if LazyGroupFinderSoundAnnouncement then PlaySound(8960, "master") end
end

function RegisteredEvents:LFG_LIST_SEARCH_RESULT_UPDATED(event, resultsID)
	if not LazyGroupFinderEnabled then return end
	--if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFRegisteredEvents:LFG_LIST_SEARCH_RESULT_UPDATED: " .. event) end
	if LFGListFrame:IsVisible() then
		LazyGroupFinderDisableAddon()
		return
	end

	local id,activity,name,comment,_,ilvl,_,age,_,_,_,isDelisted,leader,members = C_LFGList.GetSearchResultInfo(resultsID)

	local activityName,_,activityType,_,activityIlvl,_,_,_,_,_ = C_LFGList.GetActivityInfo(activity)
	
	local groupSetup = C_LFGList.GetSearchResultMemberCounts(id)
	local encounterInfo = C_LFGList.GetSearchResultEncounterInfo(id)
	local deadbosses = " "
	if encounterInfo ~= nil then
		deadbosses = #encounterInfo .. ""
	end
	
	if age ~= nil then
		for k, v in pairs(LGFfoundGroups) do
			if string.lower(name) == string.lower(LGFfoundGroups[k].Name) and string.lower(leader) ~= string.lower(LGFfoundGroups[k].Leader) then --Leader has changed
				if LGFfoundGroups[k].Id == LGFcurrentID then
					LGFcurrentID = id
					LGFcurrentLeader = leader
				end
				LGFfoundGroups[k].Id = id
				LGFfoundGroups[k].Leader = leader
				--LGFfoundGroups[k].Score = ilvl + members + math.abs(GetAverageItemLevel() - activityIlvl) * -1000
			elseif string.lower(name) ~= string.lower(LGFfoundGroups[k].Name) and string.lower(leader) == string.lower(LGFfoundGroups[k].Leader) then -- Name has changed
				if LGFfoundGroups[k].Id == LGFcurrentID then
					LGFcurrentID = id
				end
				LGFfoundGroups[k].Id = id
				LGFfoundGroups[k].Name = name
				--LGFfoundGroups[k].Score = ilvl + members + math.abs(GetAverageItemLevel() - activityIlvl) * -1000
			elseif isDelisted and string.lower(name) == string.lower(LGFfoundGroups[k].Name) and string.lower(leader) == string.lower(LGFfoundGroups[k].Leader) then -- Group delisted
				if LGFfoundGroups[k].Id == LGFcurrentID then
					-- give new popup
				end
				LGFfoundGroups[k] = nil
				LGFgroupsFound = LGFgroupsFound - 1
			elseif string.lower(name) == string.lower(LGFfoundGroups[k].Name) and string.lower(leader) == string.lower(LGFfoundGroups[k].Leader) then -- Some update
				if LGFfoundGroups[k].Id == LGFcurrentID then
					LGFcurrentID = id
					LGFcurrentActivity = activity
				end
				LGFfoundGroups[k].Id = id
				LGFfoundGroups[k].Activity = activity
				--LGFfoundGroups[k].Score = ilvl + members + math.abs(GetAverageItemLevel() - activityIlvl) * -1000
			end
		end
	end
end

function RegisteredEvents:LFG_LIST_APPLICATION_STATUS_UPDATED(event, applicationID, status)
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFRegisteredEvents:LFG_LIST_APPLICATION_STATUS_UPDATED: " .. event) end

	local _,activity,name,comment,_,ilvl,_,_,_,_,_,_,leader,members = C_LFGList.GetSearchResultInfo(applicationID)
    
	if (status == "applied") then
		if LazyGroupFinderAchievementOnApply then -- SEND ACHIEVEMNT TO LEADER
			achID = GetActivityAchievement(activity)
			if achID ~= nil then
				SendChatMessage(GetAchievementLink(tonumber(achID)), "WHISPER", nil, leader)
			end
		end
	end
end

for k, v in pairs(RegisteredEvents) do
	LazyGroupFinderAddon:RegisterEvent(k)
end

LazyGroupFinderAddon:SetScript("OnUpdate", function(self, elapsed)
	if not LazyGroupFinderEnabled then return end
	
	if LFGListFrame:IsVisible() or LazyGroupFinderOptions:IsVisible() then
		LazyGroupFinderDisableAddon()
		return
	end

	if not LazyGroupFinderPopup:IsVisible() and not LFGListApplicationDialog:IsVisible() then
		if LGFgroupsFound ~= nil then
			ShowBestResult()
		end
	end
end)