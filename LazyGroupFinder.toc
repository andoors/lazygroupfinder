## Interface: 70300
## Title: Lazy Group Finder
## Version: 1.5
## Notes: Group Finder for the lazy
## Author: Luuci-Ravencrest EU
## SavedVariablesPerCharacter: LazyGroupFinderEnabled
## SavedVariablesPerCharacter: LazyGroupFinderAchievementOnApply
## SavedVariablesPerCharacter: LazyGroupFinderActivityType
## SavedVariablesPerCharacter: LazyGroupFinderActivities
## SavedVariablesPerCharacter: LGF_ActivityFilter
## SavedVariablesPerCharacter: LGF_Keywords
## SavedVariablesPerCharacter: LGF_Bannedwords
## SavedVariablesPerCharacter: LGF_MinTank
## SavedVariablesPerCharacter: LGF_MaxTank
## SavedVariablesPerCharacter: LGF_MinHeal
## SavedVariablesPerCharacter: LGF_MaxHeal
## SavedVariablesPerCharacter: LGF_MinDps
## SavedVariablesPerCharacter: LGF_MaxDps
## SavedVariablesPerCharacter: LGF_MinIlvl
## SavedVariablesPerCharacter: LGF_ScoreFactorIlvl
## SavedVariablesPerCharacter: LGF_ScoreFactorMember
## SavedVariablesPerCharacter: LGF_ScoreFactorEncounter
## SavedVariables: LazyGroupFinderMinimapIconPoint
## SavedVariables: LazyGroupFinderMinimapIconHover
## SavedVariables: LazyGroupFinderSoundAnnouncement
## SavedVariables: LazyGroupFinderChatAnnouncement

LazyGroupFinderMapIcon.lua
LazyGroupFinder.lua
LazyGroupFinderFunc.lua
LazyGroupFinderOptions.lua
LazyGroupFinderPopup.lua