LazyGroupFinderPopup = CreateFrame("Frame", nil, UIParent)
LazyGroupFinderPopup:SetBackdrop({bgFile = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-BrokenIsles",
	edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Gold-Border",
	tile = true, tileSize = 362, edgeSize = 24,
	insets = {left = 4, right = 4, top = 0, bottom = 6}}
)
LazyGroupFinderPopup:SetSize(362, 46)
LazyGroupFinderPopup:SetPoint("TOP", UIParent, "TOP", 0, -200)
LazyGroupFinderPopup:SetFrameStrata("HIGH")
LazyGroupFinderPopup:SetMovable(true)
LazyGroupFinderPopup:EnableMouse(true)
LazyGroupFinderPopup:RegisterForDrag("LeftButton")
LazyGroupFinderPopup:SetScript("OnDragStart", LazyGroupFinderPopup.StartMoving)
LazyGroupFinderPopup:SetScript("OnDragStop", LazyGroupFinderPopup.StopMovingOrSizing)
LazyGroupFinderPopup:Hide()

	
local id,activity,name,comment,_,ilvl,_,age,_,_,_,isDelisted,leader,members
local activityName
local encounterInfo = nil
local groupSetup = nil
local popupID = nil
local _index = nil

local frame = CreateFrame("Button", "LazyGroupFinderFrame", LazyGroupFinderPopup)
frame:SetBackdrop({bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background",
	nil,
	tile = true, tileSize = 344, edgeSize = 0}
)
frame:SetSize(344, 147)
frame:SetPoint("TOP", LazyGroupFinderPopup, "TOP", 0, -9)
frame:EnableMouse(true)
frame:RegisterForClicks("LeftButtonUp", "RightButtonUp")

frame:SetScript("OnClick", function(self)
	local button = GetMouseButtonClicked()
	if button == "LeftButton" then
		LFGListApplicationDialog_Show(LFGListApplicationDialog, LGFcurrentID)
	end	
	LazyGroupFinderPopup:Hide()
	PlaySound(856)
end)

frame:SetScript("OnEnter", function(self)
	GameTooltip:SetOwner(self, "ANCHOR_BOTTOM")
	GameTooltip:SetText("|cFFFFFFFFBest match of " .. LGFgroupsFound+ 1 .. " group(s).")
	GameTooltip:AddLine("Leftclick: Sign up")
	GameTooltip:AddLine("Rightclick: Ignore")
	GameTooltip:Show()
end)
frame:SetScript("OnLeave", function(self)
	GameTooltip:Hide()
end)

local groupName = frame:CreateFontString(nil, "OVERLAY", "GameFontHighlightLarge")
groupName:SetPoint("TOPLEFT", frame, "TOPLEFT", 11, -11)
groupName:SetJustifyH("LEFT")
groupName:SetSize(306, 0)
groupName:SetText("GROUP NAME")
groupName:SetWordWrap(false)

local instanceName = frame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
instanceName:SetPoint("TOPLEFT", groupName, "BOTTOMLEFT", 1, -4)
instanceName:SetJustifyH("LEFT")
instanceName:SetSize(0, 0)
instanceName:SetText("Instance NAME")

local groupComment = frame:CreateFontString(nil, "OVERLAY", "GameFontDisable")
groupComment:SetPoint("TOPLEFT", instanceName, "BOTTOMLEFT", 0, -2)
groupComment:SetJustifyH("LEFT")
groupComment:SetSize(306, 0)
groupComment:SetText("desc")

local groupLeader = frame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
groupLeader:SetPoint("TOPLEFT", groupComment, "BOTTOMLEFT", 0, -2)
groupLeader:SetJustifyH("LEFT")
groupLeader:SetSize(0, 0)
groupLeader:SetText("leader")

local groupItemLevel = frame:CreateFontString(nil, "OVERLAY", "GameFontHighlight")
groupItemLevel:SetPoint("TOPLEFT", groupLeader, "TOPRIGHT", 8, 0)
groupItemLevel:SetJustifyH("LEFT")
groupItemLevel:SetSize(0, 0)
groupItemLevel:SetText("")

local bossesDead = frame:CreateFontString(nil, "OVERLAY", "GameFontRed")
bossesDead:SetPoint("TOPLEFT", groupLeader, "BOTTOMLEFT", 0, -2)
bossesDead:SetJustifyH("LEFT")
bossesDead:SetSize(306, 0)
bossesDead:SetText("")

local groupMembers = frame:CreateFontString(nil, "OVERLAY", "GameFontHighlight")
groupMembers:SetPoint("TOPLEFT", bossesDead, "BOTTOMLEFT", 0, -5)
groupMembers:SetJustifyH("LEFT")
groupMembers:SetSize(306, 0)
groupMembers:SetText("")

local close = CreateFrame("Button", nil, frame)
close:SetNormalTexture("Interface\\Buttons\\UI-Panel-MinimizeButton-Up")
close:SetPushedTexture("Interface\\Buttons\\UI-Panel-MinimizeButton-Down")
close:SetHighlightTexture("Interface\\Buttons\\UI-Panel-MinimizeButton-Highlight")
close:SetSize(31, 31)
close:SetPoint("TOPRIGHT", frame, "TOPRIGHT", 5, 6)
close:HookScript("OnClick", function(self)
	LazyGroupFinderDisableAddon()
	PlaySound(856)
end)

local bg2 = CreateFrame("Frame", nil, frame)
bg2:SetBackdrop({bgFile = "Interface\\COMMON\\bluemenu-goldborder-horiz",
	edgeFile = nil,
	tile = true, tileSize = 47, edgeSize = 32}
)
bg2:SetSize(344, 47)
bg2:SetPoint("BOTTOM", frame, "BOTTOM", 0, 0)
bg2:EnableMouse(true)

local groupScore = frame:CreateFontString(nil, "OVERLAY", "GameFontDisable")
groupScore:SetPoint("BOTTOMRIGHT", bg2, "TOPRIGHT", 1, 1)
groupScore:SetJustifyH("RIGHT")
groupScore:SetSize(100, 0)
groupScore:SetText("")

local signup = CreateFrame("Button", nil, bg2, "UIPanelButtonTemplate")
signup:SetSize(100, 21)
signup:SetPoint("RIGHT", -50, 0)
signup:SetText("SIGN UP")
signup:HookScript("OnClick", function(self)
	LFGListApplicationDialog_Show(LFGListApplicationDialog, LGFcurrentID)
	LazyGroupFinderPopup:Hide()
	PlaySound(856)
end)

local ignore = CreateFrame("Button", nil, bg2, "UIPanelButtonTemplate")
ignore:SetSize(100, 21)
ignore:SetPoint("LEFT", 50, 0)
ignore:SetText("IGNORE")
ignore:HookScript("OnClick", function(self)
	LazyGroupFinderPopup:Hide()
	PlaySound(856)
end)

function LazyGroupFinder_GroupPopup(groupID, Score)
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFLazyGroupFinder_GroupPopup") end
	popupID = groupID
	
	id,activity,name,comment,_,ilvl,_,age,_,_,_,isDelisted,leader,members = C_LFGList.GetSearchResultInfo(popupID)
	LGFcurrentID = id
	LGFcurrentLeader = leader
	LGFcurrentActivity = activity
	groupSetup = C_LFGList.GetSearchResultMemberCounts(id)
	encounterInfo = C_LFGList.GetSearchResultEncounterInfo(id)
	
	local _, lootableEncounters, encountersLeft = GetEncounterScore(activity, encounterInfo)
	
	local lockedBosses = {}
	if LGFlockoutIDs[activity] ~= nil then
		local savedInstances = GetNumSavedInstances()
		for i=1,savedInstances do
			local instanceName, instanceID, instanceReset, instanceDifficulty, locked, extended, instanceIDMostSig, isRaid, maxPlayers, difficultyName, maxBosses, defeatedBosses = GetSavedInstanceInfo(i)
			if locked and LGFlockoutIDs[activity] == (instanceName .. instanceDifficulty) then
				for j=1,maxBosses do
					local bossName, fileDataID, isKilled, unknown4 = GetSavedInstanceEncounterInfo(i, j)
					if isKilled then
						lockedBosses[bossName] = true
					end
				end
			end
		end
	end
	
	local deadbosses = ""
	if encounterInfo ~= nil then
		for k, v in pairs(encounterInfo) do
			local lockedOut = ""
			if lockedBosses[v] ~= nil then 
				lockedOut = "\124TInterface\\RaidFrame\\ReadyCheck-Ready:14:14\124t"
			else
				lockedOut = "\124TInterface\\RaidFrame\\ReadyCheck-NotReady:14:14\124t"
			end
			deadbosses = deadbosses .. "\n" .. lockedOut .. "  " .. v .. "  "
		end
	end
	if lootableEncounters ~= nil then
		deadbosses = deadbosses .. "\n\n" .. "|cFFFFFFFF" .. lootableEncounters .. "/" .. encountersLeft .. " lootable bosses."
	end

	activityName = C_LFGList.GetActivityInfo(activity)

	if ilvl > 0 then 
		groupItemLevel:SetText("|cFF00FFFF" .. ilvl .. "+") 
	else
		groupItemLevel:SetText("")
	end
	
	groupName:SetText(name)
	
	instanceName:SetText(activityName)
	if comment ~= "" then groupComment:SetText("|cFFAAAAAA\"" .. comment .. "\"\n") else groupComment:SetText("") end
	groupLeader:SetText("|cFFFFFFFF" .. leader)
	groupMembers:SetText(groupSetup["TANK"] .. " \124TInterface\\LFGFRAME\\UI-LFG-ICON-PORTRAITROLES:18:18:0:-1:64:64:0:18:22:40\124t  " .. groupSetup["HEALER"] .. " \124TInterface\\LFGFRAME\\UI-LFG-ICON-PORTRAITROLES:18:18:0:-1:64:64:20:38:1:19\124t  " .. groupSetup["DAMAGER"] .. " \124TInterface\\LFGFRAME\\UI-LFG-ICON-PORTRAITROLES:18:18:0:-1:64:64:20:38:22:40\124t  (" .. members .. ")")
	bossesDead:SetText(deadbosses)
	groupScore:SetText(Score)
	
	frame:SetSize(350, groupName:GetStringHeight() + instanceName:GetStringHeight() + groupComment:GetStringHeight() + groupLeader:GetStringHeight() + bossesDead:GetStringHeight() + groupMembers:GetStringHeight() + 83)
	frame:Show()
	LazyGroupFinderPopup:SetBackdrop({bgFile = GetActivityBackground(activity),
		edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Gold-Border",
		tile = true, tileSize = 362, edgeSize = 24,
		insets = {left = 4, right = 4, top = 0, bottom = 6}}
	)
	
	LazyGroupFinderPopup:SetSize(362, frame:GetHeight() + 60 - 45)
	LazyGroupFinderPopup:Show()
end