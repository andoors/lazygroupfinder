LGFlockoutIDs = {
	[483] = "Antorus, the Burning Throne15", -- Antorus, the Burning Throne (Heroic)
	[482] = "Antorus, the Burning Throne14", -- Antorus, the Burning Throne (Normal)
	[478] = "Tomb of Sargeras15", -- Tomb of Sargeras (Heroic)
	[479] = "Tomb of Sargeras14", -- Tomb of Sargeras (Normal)
	[416] = "The Nighthold15", -- Nighthold (Heroic)
	[415] = "The Nighthold14", -- Nighthold (Normal)
	[457] = "Trial of valor15", -- Trial of valor (Heroic) 
	[456] = "Trial of valor14", -- Trial of valor (Normal)
	[414] = "The Emerald Nightmare15", -- Emerald Nightmare (Heroic)
	[413] = "The Emerald Nightmare14", -- Emerald Nightmare (Normal)
}

LGFachievementIDs = {
-- Mythic +
	[486] = "11162 11185 11184 11183", -- Seat of the Triumvirate (Mythic Keystone)
	[459] = "11162 11185 11184 11183", -- Eye of Azshara (Mythic Keystone)
	[460] = "11162 11185 11184 11183", -- Darkheart Thicket (Mythic Keystone)
	[461] = "11162 11185 11184 11183", -- Halls of Valor (Mythic Keystone)
	[462] = "11162 11185 11184 11183", -- Neltharion's Lair (Mythic Keystone)
	[463] = "11162 11185 11184 11183", -- Black Rook Hold (Mythic Keystone)
	[464] = "11162 11185 11184 11183", -- Vault of the Wardens (Mythic Keystone)
	[465] = "11162 11185 11184 11183", -- Maw of Souls (Mythic Keystone)
	[466] = "11162 11185 11184 11183", -- Court of Stars (Mythic Keystone)
	[467] = "11162 11185 11184 11183", -- The Arcway (Mythic Keystone)
	[471] = "11162 11185 11184 11183", -- Lower Karazhan (Mythic Keystone)
	[473] = "11162 11185 11184 11183", -- Upper Karazhan (Mythic Keystone)
	[476] = "11162 11185 11184 11183", -- Cathedral of Eternal Night (Mythic Keystone)
-- Raids
	[483] = "12110 11991", -- Antorus, the Burning Throne (Heroic)
	[482] = "12110 11991", -- Antorus, the Burning Throne (Normal)
	[478] = "11874 11790", -- Tomb of Sargeras (Heroic)
	[479] = "11874 11790", -- Tomb of Sargeras (Normal)
	[416] = "11195 10839", -- Nighthold (Heroic)
	[415] = "11195 10839", -- Nighthold (Normal)
	[457] = "11426 11394", -- Trial of valor (Heroic) 
	[456] = "11426 11394", -- Trial of valor (Normal)
	[414] = "11191 11194 10820", -- Emerald Nightmare (Heroic)
	[413] = "11191 11194 10820", -- Emerald Nightmare (Normal)
}

function GetActivityAchievement(id)
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFGetActivityAchievement: " .. id) end
	
	if LGFachievementIDs[id] ~= nil then
		for achID in string.gmatch(LGFachievementIDs[id], '([^ ]+)') do
			local _,_,_,completed = GetAchievementInfo(tonumber(achID))
			if completed then
				return tonumber(achID)
			end
		end
	end
	return nil
end

LGFactivityBackgroundIDs = {
	[483] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-Antorus", -- Antorus, the Burning Throne (Heroic)
	[482] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-Antorus", -- Antorus, the Burning Throne (Normal)
	[478] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TombofSargeras", -- Tomb of Sargeras (Heroic)
	[479] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TombofSargeras", -- Tomb of Sargeras (Normal)
	[416] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TheNighthold", -- Nighthold (Heroic)
	[415] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TheNighthold", -- Nighthold (Normal)
	[457] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TrialofValor", -- Trial of valor (Heroic) 
	[456] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TrialofValor", -- Trial of valor (Normal)
	[468] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TheEmeraldNightmare", -- Emerald Nightmare (Mythic)
	[414] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TheEmeraldNightmare", -- Emerald Nightmare (Heroic)
	[413] = "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-TheEmeraldNightmare", -- Emerald Nightmare (Normal)
	
}

function GetActivityBackground(id)
	if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFGetActivityBackground: " .. id) end
	
	if LGFactivityBackgroundIDs[id] ~= nil then
		return LGFactivityBackgroundIDs[id]
	else
		return "Interface\\ENCOUNTERJOURNAL\\UI-EJ-BACKGROUND-BrokenIsles"
	end
end

function IsKeywordsInWords(keywords, words)
	--if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFIsKeywordsInWords") end
	
	if keywords == "" then return true end
	for keyword in string.gmatch(string.lower(keywords), '([^ ]+)') do
		if string.match(string.lower(words), keyword) then
			return true
		end
	end
	return false
end

function IsKeywordsNotInWords(keywords, words)
	--if LGFdebug then DEFAULT_CHAT_FRAME:AddMessage("|cFFFF00FFIsKeywordsNotInWords") end
	
	if keywords == "" then return true end
	for keyword in string.gmatch(string.lower(keywords), '([^ ]+)') do
		if string.match(string.lower(words), keyword) then
			return false
		end
	end
	return true
end

function GetEncounterScore(activity, encounterInfo)

	local lockedBosses = {}
	local encounterScore = 0.0
	local lootableEncounters = 0
	local encountersLeft = 0
	local maximumBosses = 1
	
	if LGFlockoutIDs[activity] ~= nil then
		local savedInstances = GetNumSavedInstances()
		for i=1,savedInstances do
			local instanceName, instanceID, instanceReset, instanceDifficulty, locked, extended, instanceIDMostSig, isRaid, maxPlayers, difficultyName, maxBosses, defeatedBosses = GetSavedInstanceInfo(i)
			if LGFlockoutIDs[activity] == (instanceName .. instanceDifficulty) then
				maximumBosses = maxBosses
				encounterScore = maximumBosses
				encountersLeft = maximumBosses
				lootableEncounters = maximumBosses
				if locked then
					for j=1,maxBosses do
						local bossName, fileDataID, isKilled, unknown4 = GetSavedInstanceEncounterInfo(i, j)
						if isKilled then
							lockedBosses[bossName] = true
							encounterScore = encounterScore - 1
							lootableEncounters = lootableEncounters - 1
						else
							lockedBosses[bossName] = false
						end
					end
				end
			end
		end
	else
		
	end
	if encounterInfo ~= nil then
		if maximumBosses == 1 then
			encounterScore = 1
			lootableEncounters = nil
		else
			for k, v in pairs(encounterInfo) do
				encountersLeft = encountersLeft - 1
				lootableEncounters = lootableEncounters - 1
				if lockedBosses[v] ~= nil then 
					if lockedBosses[v] then
						encounterScore = encounterScore + 1
						lootableEncounters = lootableEncounters + 1
					else
						encounterScore = encounterScore - 1
					end
				end
			end
		end
	end
	encounterScore = (encounterScore / maximumBosses)
	
	return encounterScore, lootableEncounters, encountersLeft
end