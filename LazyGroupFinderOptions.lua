LazyGroupFinderOptions = CreateFrame("Frame", "LazyGroupFinderOptions", InterefaceOptionsFramePanelContainer)
LazyGroupFinderOptions.name = "Lazy Group Finder"
LazyGroupFinderOptions:Hide()

local categoryStates = {"Questing", "Dungeons", "Raids"}
local activityStates = {}
local selectedNrOfActivities = 0

-- ADDON NAME
local addonNameText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
addonNameText:SetPoint("TOPLEFT", 32, -32)
addonNameText:SetText("Lazy Group Finder")

local addonAuthor = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontDisable")
addonAuthor:SetPoint("RIGHT", LazyGroupFinderOptions, "BOTTOMRIGHT", -32, 32)
addonAuthor:SetText("created by Luuci-Ravencrest EU")

-- ACTIVITIES
local activityText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
activityText:SetPoint("TOPLEFT", 252, -55)
activityText:SetText("Activity:")

local activityStateMenu = CreateFrame("Button", "LazyGroupFinderActivityStateMenu", LazyGroupFinderOptions, "UIDropDownMenuTemplate")
activityStateMenu:SetPoint("TOPLEFT", activityText, "BOTTOMLEFT", 0, -5)
local function Initialize_ActivityStateMenu(self, level)
	local info = UIDropDownMenu_CreateInfo()
	selectedNrOfActivities = 0
	for k,v in pairs(activityStates) do
		local activityName,_,activityType,_,_,_,_,_,_,_ = C_LFGList.GetActivityInfo(k)
		if string.match(string.lower(activityName), string.lower(LGF_ActivityFilter[LazyGroupFinderActivityType])) then
			info = UIDropDownMenu_CreateInfo()
			info.text = activityName
			info.value = k
			info.checked = v
			info.func = activityState_OnClick
			info.isNotRadio = true
			info.keepShownOnClick = true
			UIDropDownMenu_AddButton(info, level)
		end
		if v then selectedNrOfActivities = selectedNrOfActivities + 1 end
	end
	UIDropDownMenu_SetText(activityStateMenu, "Filter (" .. selectedNrOfActivities .. ")")
end

function activityState_OnClick(self)
	LazyGroupFinderActivities[self.value] = not LazyGroupFinderActivities[self.value]
	activityStates[self.value] = LazyGroupFinderActivities[self.value]
	if activityStates[self.value] then selectedNrOfActivities = selectedNrOfActivities + 1 end
	if not activityStates[self.value] then selectedNrOfActivities = selectedNrOfActivities - 1 end
	UIDropDownMenu_SetText(activityStateMenu, "Filter (" .. selectedNrOfActivities .. ")")
end

local activityStateMenuFilterText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontDisable")
activityStateMenuFilterText:SetPoint("TOPLEFT", 432, -55)
activityStateMenuFilterText:SetText("Filter:")

local activityStateMenuFilter = CreateFrame("EditBox", nil, LazyGroupFinderOptions, "InputBoxTemplate")
activityStateMenuFilter:SetPoint("TOPLEFT", activityStateMenuFilterText, "BOTTOMLEFT", 5, -5)
activityStateMenuFilter:SetAutoFocus(false)
activityStateMenuFilter:SetSize(100, 25)
activityStateMenuFilter:SetText("")
activityStateMenuFilter:SetMaxLetters(100)
activityStateMenuFilter:SetScript("OnTextChanged", function(self)
	LGF_ActivityFilter[LazyGroupFinderActivityType] = self:GetText()
	Initialize_ActivityStateMenu()
end)

UIDropDownMenu_SetWidth(activityStateMenu, 130)
UIDropDownMenu_SetButtonWidth(activityStateMenu, 130)
UIDropDownMenu_JustifyText(activityStateMenu, "CENTER")
UIDropDownMenu_Initialize(activityStateMenu, Initialize_ActivityStateMenu)

-- CATEGORY
local categoryText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
categoryText:SetPoint("TOPLEFT", 42, -55)
categoryText:SetText("Category:")

local categoryStateMenu = CreateFrame("Button", "LazyGroupFinderCategoryStateMenu", LazyGroupFinderOptions, "UIDropDownMenuTemplate")
categoryStateMenu:SetPoint("TOPLEFT", categoryText, "BOTTOMLEFT", 0, -5)
local function Initialize_CategoryStateMenu(self, level)
	local info = UIDropDownMenu_CreateInfo()
	for k,v in pairs(categoryStates) do
		info = UIDropDownMenu_CreateInfo()
		info.text = v
		info.value = k
		info.func = categoryState_OnClick
		UIDropDownMenu_AddButton(info, level)
	end
end

function categoryState_OnClick(self)
	UIDropDownMenu_SetSelectedID(categoryStateMenu, self:GetID())
	LazyGroupFinderActivityType = self.value
	UpdateDialog()
end

UIDropDownMenu_SetWidth(categoryStateMenu, 130)
UIDropDownMenu_SetButtonWidth(categoryStateMenu, 130)
UIDropDownMenu_JustifyText(categoryStateMenu, "CENTER")
UIDropDownMenu_Initialize(categoryStateMenu, Initialize_CategoryStateMenu)

-- KEYWORDS
local keywordsText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
keywordsText:SetPoint("TOPLEFT", 42, -115)
keywordsText:SetText("Filter keywords:")

local keywordsDESCText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontDisable")
keywordsDESCText:SetPoint("LEFT", keywordsText, "RIGHT", 20, 0)
keywordsDESCText:SetText("for Name, Description or Leader (one must match)")

local keywordsEditBox = CreateFrame("EditBox", "LGF_Keywords[LazyGroupFinderActivityType]EditBox", LazyGroupFinderOptions, "InputBoxTemplate")
keywordsEditBox:SetPoint("TOPLEFT", keywordsText, "BOTTOMLEFT", 20, -5)
keywordsEditBox:SetAutoFocus(false)
keywordsEditBox:SetSize(300, 25)
keywordsEditBox:SetText("")
keywordsEditBox:SetScript("OnTextChanged", function(self)
	LGF_Keywords[LazyGroupFinderActivityType] = keywordsEditBox:GetText()
end)

-- IGNORE WORDS
local bannedwordsText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
bannedwordsText:SetPoint("TOPLEFT", 42, -165)
bannedwordsText:SetText("Banned keywords:")

local bannedwordsDESCText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontDisable")
bannedwordsDESCText:SetPoint("LEFT", bannedwordsText, "RIGHT", 20, 0)
bannedwordsDESCText:SetText("for Name, Description or Leader (none can match)")

local bannedwordsEditBox = CreateFrame("EditBox", "LGF_Bannedwords[LazyGroupFinderActivityType]EditBox", LazyGroupFinderOptions, "InputBoxTemplate")
bannedwordsEditBox:SetPoint("TOPLEFT", bannedwordsText, "BOTTOMLEFT", 20, -5)
bannedwordsEditBox:SetAutoFocus(false)
bannedwordsEditBox:SetSize(300, 25)
bannedwordsEditBox:SetText("")
bannedwordsEditBox:SetScript("OnTextChanged", function(self)
	LGF_Bannedwords[LazyGroupFinderActivityType] = bannedwordsEditBox:GetText()
end)

-- GROUP SETUP WORDS
local groupsetupText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
groupsetupText:SetPoint("TOPLEFT", 42, -215)
groupsetupText:SetText("Group setup:")

local groupsetupDESCText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontDisable")
groupsetupDESCText:SetPoint("LEFT", groupsetupText, "RIGHT", 20, 0)
groupsetupDESCText:SetText("(min - max) leave empty for no restriction")

local icontankText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
icontankText:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 20, -9)
icontankText:SetText("\124TInterface\\LFGFRAME\\UI-LFG-ICON-PORTRAITROLES:18:18:0:-1:64:64:0:18:22:40\124t        -        ")

local mintankEditBox = CreateFrame("EditBox", nil, LazyGroupFinderOptions, "InputBoxTemplate")
mintankEditBox:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 45, -5)
mintankEditBox:SetAutoFocus(false)
mintankEditBox:SetSize(20, 25)
mintankEditBox:SetText("")
mintankEditBox:SetMaxLetters(2)
mintankEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_MinTank[LazyGroupFinderActivityType] = ""
	elseif tonumber(self:GetText()) ~= nil then LGF_MinTank[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local maxtankEditBox = CreateFrame("EditBox", nil, LazyGroupFinderOptions, "InputBoxTemplate")
maxtankEditBox:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 85, -5)
maxtankEditBox:SetAutoFocus(false)
maxtankEditBox:SetSize(20, 25)
maxtankEditBox:SetText("")
maxtankEditBox:SetMaxLetters(2)
maxtankEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_MaxTank[LazyGroupFinderActivityType] = ""
	elseif tonumber(self:GetText()) ~= nil then LGF_MaxTank[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local iconhealText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
iconhealText:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 125, -9)
iconhealText:SetText("\124TInterface\\LFGFRAME\\UI-LFG-ICON-PORTRAITROLES:18:18:0:-1:64:64:20:38:1:19\124t        -        ")

local minhealEditBox = CreateFrame("EditBox", nil, LazyGroupFinderOptions, "InputBoxTemplate")
minhealEditBox:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 150, -5)
minhealEditBox:SetAutoFocus(false)
minhealEditBox:SetSize(20, 25)
minhealEditBox:SetText("")
minhealEditBox:SetMaxLetters(2)
minhealEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_MinHeal[LazyGroupFinderActivityType] = ""
	elseif tonumber(self:GetText()) ~= nil then LGF_MinHeal[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local maxhealEditBox = CreateFrame("EditBox", nil, LazyGroupFinderOptions, "InputBoxTemplate")
maxhealEditBox:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 190, -5)
maxhealEditBox:SetAutoFocus(false)
maxhealEditBox:SetSize(20, 25)
maxhealEditBox:SetText("")
maxhealEditBox:SetMaxLetters(2)
maxhealEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_MaxHeal[LazyGroupFinderActivityType] = ""
	elseif tonumber(self:GetText()) ~= nil then LGF_MaxHeal[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local icondpsText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
icondpsText:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 230, -9)
icondpsText:SetText("\124TInterface\\LFGFRAME\\UI-LFG-ICON-PORTRAITROLES:18:18:0:-1:64:64:20:38:22:40\124t        -        ")

local mindpsEditBox = CreateFrame("EditBox", nil, LazyGroupFinderOptions, "InputBoxTemplate")
mindpsEditBox:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 255, -5)
mindpsEditBox:SetAutoFocus(false)
mindpsEditBox:SetSize(20, 25)
mindpsEditBox:SetText("")
mindpsEditBox:SetMaxLetters(2)
mindpsEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_MinDps[LazyGroupFinderActivityType] = ""
	elseif tonumber(self:GetText()) ~= nil then LGF_MinDps[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local maxdpsEditBox = CreateFrame("EditBox", nil, LazyGroupFinderOptions, "InputBoxTemplate")
maxdpsEditBox:SetPoint("TOPLEFT", groupsetupText, "BOTTOMLEFT", 295, -5)
maxdpsEditBox:SetAutoFocus(false)
maxdpsEditBox:SetSize(20, 25)
maxdpsEditBox:SetText("")
maxdpsEditBox:SetMaxLetters(2)
maxdpsEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_MaxDps[LazyGroupFinderActivityType] = ""
	elseif tonumber(self:GetText()) ~= nil then LGF_MaxDps[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

-- Min Ilvl
local minilvlText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
minilvlText:SetPoint("TOPLEFT", 42, -265)
minilvlText:SetText("Minimum item level:")

local minilvlDESCText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontDisable")
minilvlDESCText:SetPoint("LEFT", minilvlText, "RIGHT", 20, 0)
minilvlDESCText:SetText("")

local minilvlEditBox = CreateFrame("EditBox", "LGF_Bannedwords[LazyGroupFinderActivityType]EditBox", LazyGroupFinderOptions, "InputBoxTemplate")
minilvlEditBox:SetPoint("TOPLEFT", minilvlText, "BOTTOMLEFT", 20, -5)
minilvlEditBox:SetAutoFocus(false)
minilvlEditBox:SetSize(40, 25)
minilvlEditBox:SetText("")
minilvlEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_MinIlvl[LazyGroupFinderActivityType] = 0
	elseif tonumber(self:GetText()) ~= nil then LGF_MinIlvl[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

-- Score factors
local scoreFactorsText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
scoreFactorsText:SetPoint("TOPLEFT", 42, -315)
scoreFactorsText:SetText("Score factors:")

local ilvlFactorsEditBox = CreateFrame("EditBox", "LGF_Bannedwords[LazyGroupFinderActivityType]EditBox", LazyGroupFinderOptions, "InputBoxTemplate")
ilvlFactorsEditBox:SetPoint("TOPLEFT", scoreFactorsText, "BOTTOMLEFT", 20, -5)
ilvlFactorsEditBox:SetAutoFocus(false)
ilvlFactorsEditBox:SetSize(25, 25)
ilvlFactorsEditBox:SetMaxLetters(3)
ilvlFactorsEditBox:SetText("")
ilvlFactorsEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_ScoreFactorIlvl[LazyGroupFinderActivityType] = 1.0
	elseif tonumber(self:GetText()) ~= nil then LGF_ScoreFactorIlvl[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local ilvlFactorsText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
ilvlFactorsText:SetPoint("LEFT", ilvlFactorsEditBox, "RIGHT", 5, 0)
ilvlFactorsText:SetText("* ilvl + ")

local memberFactorsEditBox = CreateFrame("EditBox", "LGF_Bannedwords[LazyGroupFinderActivityType]EditBox", LazyGroupFinderOptions, "InputBoxTemplate")
memberFactorsEditBox:SetPoint("LEFT", ilvlFactorsText, "RIGHT", 5, 0)
memberFactorsEditBox:SetAutoFocus(false)
memberFactorsEditBox:SetSize(25, 25)
memberFactorsEditBox:SetMaxLetters(3)
memberFactorsEditBox:SetText("")
memberFactorsEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_ScoreFactorMember[LazyGroupFinderActivityType] = 1.0
	elseif tonumber(self:GetText()) ~= nil then LGF_ScoreFactorMember[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local memberFactorsText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
memberFactorsText:SetPoint("LEFT", memberFactorsEditBox, "RIGHT", 5, 0)
memberFactorsText:SetText("* members + ")

local encounterFactorsEditBox = CreateFrame("EditBox", "LGF_Bannedwords[LazyGroupFinderActivityType]EditBox", LazyGroupFinderOptions, "InputBoxTemplate")
encounterFactorsEditBox:SetPoint("LEFT", memberFactorsText, "RIGHT", 5, 0)
encounterFactorsEditBox:SetAutoFocus(false)
encounterFactorsEditBox:SetSize(25, 25)
encounterFactorsEditBox:SetMaxLetters(3)
encounterFactorsEditBox:SetText("")
encounterFactorsEditBox:SetScript("OnTextChanged", function(self)
	if self:GetText() == "" then LGF_ScoreFactorEncounter[LazyGroupFinderActivityType] = 1.0
	elseif tonumber(self:GetText()) ~= nil then LGF_ScoreFactorEncounter[LazyGroupFinderActivityType] = self:GetText()
	else self:SetText("") end
end)

local encounterFactorsText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
encounterFactorsText:SetPoint("LEFT", encounterFactorsEditBox, "RIGHT", 5, 0)
encounterFactorsText:SetText("* encounter score")

-- ADDON SETTINGS NAME
local bonusNameText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
bonusNameText:SetPoint("TOPLEFT", 32, -380)
bonusNameText:SetText("Addon Settings")

-- ACHIEVEMENT ON WHISPER
local achievementButton = CreateFrame("CheckButton", "LazyGroupFinderAchievementCheckButton", LazyGroupFinderOptions, "OptionsCheckButtonTemplate")
achievementButton:SetSize(26, 26)
achievementButton:SetPoint("LEFT", bonusNameText, "TOPLEFT", 0, -33)
achievementButton:HookScript("OnClick", function(self)
	if self:GetChecked() then
		LazyGroupFinderAchievementOnApply = true
		PlaySound(856)
	else
		LazyGroupFinderAchievementOnApply = false
		PlaySound(857)
	end
end)

local achievementText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
achievementText:SetPoint("TOPLEFT", achievementButton, "TOPLEFT", 30, -7)
achievementText:SetText("Auto whisper achievement")

-- SOUND ANNOUNCEMENT
local soundButton = CreateFrame("CheckButton", "LazyGroupFinderSoundCheckButton", LazyGroupFinderOptions, "OptionsCheckButtonTemplate")
soundButton:SetSize(26, 26)
soundButton:SetPoint("LEFT", bonusNameText, "TOPLEFT", 0, -63)
soundButton:HookScript("OnClick", function(self)
	if self:GetChecked() then
		LazyGroupFinderSoundAnnouncement = true
		PlaySound(856)
	else
		LazyGroupFinderSoundAnnouncement = false
		PlaySound(857)
	end
end)

local soundText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
soundText:SetPoint("TOPLEFT", soundButton, "TOPLEFT", 30, -7)
soundText:SetText("Sound announcement")

-- CHAT ANNOUNCEMENT
local chatButton = CreateFrame("CheckButton", "LazyGroupFinderChatCheckButton", LazyGroupFinderOptions, "OptionsCheckButtonTemplate")
chatButton:SetSize(26, 26)
chatButton:SetPoint("LEFT", bonusNameText, "TOPLEFT", 0, -93)
chatButton:HookScript("OnClick", function(self)
	if self:GetChecked() then
		LazyGroupFinderChatAnnouncement = true
		PlaySound(856)
	else
		LazyGroupFinderChatAnnouncement = false
		PlaySound(857)
	end
end)

local chatText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
chatText:SetPoint("TOPLEFT", chatButton, "TOPLEFT", 30, -7)
chatText:SetText("Chat announcement")

-- MINIMAP HOVER ICON
local minimapHoverButton = CreateFrame("CheckButton", "LazyGroupFinderChatCheckButton", LazyGroupFinderOptions, "OptionsCheckButtonTemplate")
minimapHoverButton:SetSize(26, 26)
minimapHoverButton:SetPoint("LEFT", bonusNameText, "TOPLEFT", 0, -123)
minimapHoverButton:HookScript("OnClick", function(self)
	if self:GetChecked() then
		LazyGroupFinderMinimapIconHover = true
		LGFminimapIconButton:Hide()
		PlaySound(856)
	else
		LazyGroupFinderMinimapIconHover = false
		LGFminimapIconButton:Show()
		PlaySound(857)
	end
end)

local minimapHoverText = LazyGroupFinderOptions:CreateFontString(nil, "ARTWORK", "GameFontHighlight")
minimapHoverText:SetPoint("TOPLEFT", minimapHoverButton, "TOPLEFT", 30, -7)
minimapHoverText:SetText("Show only minimap icon on hover")

-- ON SHOW
function UpdateDialog()
	activityStates = {}
	local results = C_LFGList.GetAvailableActivities(LazyGroupFinderActivityType, nil, 1)
	for k,v in pairs(results) do
		if LazyGroupFinderActivities[v] == nil then LazyGroupFinderActivities[v] = false end
		activityStates[v] = LazyGroupFinderActivities[v]
	end
	Initialize_ActivityStateMenu()
	activityStateMenuFilter:SetText(LGF_ActivityFilter[LazyGroupFinderActivityType])
	keywordsEditBox:SetText(LGF_Keywords[LazyGroupFinderActivityType])
	bannedwordsEditBox:SetText(LGF_Bannedwords[LazyGroupFinderActivityType])
	achievementButton:SetChecked(LazyGroupFinderAchievementOnApply)
	soundButton:SetChecked(LazyGroupFinderSoundAnnouncement)
	chatButton:SetChecked(LazyGroupFinderChatAnnouncement)
	minimapHoverButton:SetChecked(LazyGroupFinderMinimapIconHover)
	mintankEditBox:SetText(LGF_MinTank[LazyGroupFinderActivityType])
	maxtankEditBox:SetText(LGF_MaxTank[LazyGroupFinderActivityType])
	minhealEditBox:SetText(LGF_MinHeal[LazyGroupFinderActivityType])
	maxhealEditBox:SetText(LGF_MaxHeal[LazyGroupFinderActivityType])
	mindpsEditBox:SetText(LGF_MinDps[LazyGroupFinderActivityType])
	maxdpsEditBox:SetText(LGF_MaxDps[LazyGroupFinderActivityType])
	minilvlEditBox:SetText(LGF_MinIlvl[LazyGroupFinderActivityType])
	ilvlFactorsEditBox:SetText(LGF_ScoreFactorIlvl[LazyGroupFinderActivityType])
	memberFactorsEditBox:SetText(LGF_ScoreFactorMember[LazyGroupFinderActivityType])
	encounterFactorsEditBox:SetText(LGF_ScoreFactorEncounter[LazyGroupFinderActivityType])
end

LazyGroupFinderOptions:SetScript("OnShow", function(...)
	Initialize_CategoryStateMenu()
	UIDropDownMenu_SetSelectedName(categoryStateMenu, categoryStates[LazyGroupFinderActivityType])
	UpdateDialog()
end)

InterfaceOptions_AddCategory(LazyGroupFinderOptions)
